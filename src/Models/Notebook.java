package Models;

import java.util.ArrayList;

/**
 * The main class, contains methods to add/remove notes (string type) and more
 *
 * @since 28/09/2017
 * @author Bereketab Gulai
 */
public class Notebook implements Notable {

    // Global attributes
    private ArrayList<String> notes;
    private int count; // Need to keep track of Arraylist

    // Default constructor
    public Notebook() {
        DbHandler dbHandler = new DbHandler();
        
        // Only assign db notes if it is not empty
        notes = dbHandler.isEmpty() ? new ArrayList<>() : dbHandler.getNotes();
        
        count = notes.size();
    }

    // Getters and Setters
    @Override
    public ArrayList<String> getNotes() {
        // As long as app is running it will have the same values as the db
        return notes;
    }

    @Override
    public void setNotes(ArrayList<String> notes) {
        this.notes = notes;
    }

    // Methods
    @Override
    public void add(String note) {
        if (!isEmpty()) {
            // Make this does not go below 0
            for (int i = count; i > 0; i--) {

                // Make sure the following happens only at the beginning of loop
                if (i == count) {
                    // Create a new element with copy of the last element
                    notes.add(i, notes.get(i - 1));
                }

                // The rest of elements must replace the existing element indices
                notes.set(i, notes.get(i - 1));
            }

            // Replace index 0 with new element content(note)
            notes.set(0, note);
            count++;
            saveToDb(note);
        } else {
            notes.add(note);
            count++;
            saveToDb(note);
        }

    }

    @Override
    public void addAt(int noteIndex, String note) {
        if (!isEmpty()) {
            // Replace index 0 with new element content(note)
            notes.set(0, note);
        }
    }

    @Override
    public void remove(int noteIndex) {
        notes.remove(noteIndex);
        count--;
    }

    @Override
    public int getSize() {
        return this.notes.size();
    }

    @Override
    public boolean isEmpty() {
        return notes.isEmpty();
    }

    private boolean saveToDb(String note) {
        DbHandler dbHandler = new DbHandler();
        return dbHandler.save(note);
    }
}
