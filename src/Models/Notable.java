package Models;

import java.util.ArrayList;

/**
 * Interface for Notebook class
 *
 * 05/10/2017 
 * Bereketab Gulai 
 * ProjectNotebook Models
 */
public interface Notable {

    /**
     * @return returns the all notes
     */
    public ArrayList<String> getNotes();

    /**
     * Takes arraysList of strings and assigns it to notes
     *
     * @param notes the value to store
     */
    public void setNotes(ArrayList<String> notes);

    /**
     * Adds to the top of the notes
     *
     * @param note is added notes
     */
    public void add(String note);

    /**
     * Replaces an existing index
     *
     * @param note is added at the @param index
     */
    public void addAt(int noteIndex, String note);

    /**
     * Takes index and removes it from the notes
     *
     * @param noteIndex used to access the item to be removed
     */
    public void remove(int noteIndex);

    /**
     * @return returns the size of the notes
     */
    public int getSize();

    /**
     * @return returns whether the notes is empty or not
     */
    public boolean isEmpty();
}
