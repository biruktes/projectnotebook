package Models;

/**
 * Contains methods to help to concatenate and de-concatenate a string
 *
 * @since 05/10/2017
 * @author Bereketab Gulai
 */
public final class StringUtility {

    // Joins the title and note content
    public static String concatNote(String title, String noteContent) {
        return title + "|" + noteContent;
    }

    // Unjoins the title and note content
    public static String[] deConcatNote(String note) {
        // Gets the 
        String[] strNote = new String[2];

        strNote[0] = note.substring(0, note.indexOf("|"));
        // +1 to desclude the pipe
        strNote[1] = note.substring(note.indexOf("|") + 1, note.length());

        return strNote;
    }
}
