package Models;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.net.InetAddress;

/**
 * The main application, includes all components and events
 *
 * @since 19/10/2017
 * @author Bereketab Gulai
 */

public class DbHandler implements DbHandlable {

    // Db constant 
    private final String NOTES_TABLE = "NotesTable";
    // Connection Constants
    private final String DRIVER = "org.apache.derby.jdbc.ClientDriver";
    private final String JDBC_URL = "jdbc:derby://localhost:1527/Notes";
    private final String USER_NAME = "biruk";
    private final String USER_PASSWORD = "root";

    // Default constructor, initailises db
    public DbHandler() {
        try {
            Class.forName(DRIVER);
        } catch ( ClassNotFoundException ex) {
            Logger.getLogger(DbHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public ArrayList<String> getNotes() {
        ArrayList<String> notes = new ArrayList<>();

        try {
            // Open connection
            Connection conn = DriverManager.getConnection(JDBC_URL, USER_NAME, USER_PASSWORD);

            String GetAllNotesQry = "SELECT * FROM " + NOTES_TABLE;
            Statement sqlStmt = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);

            ResultSet dbNotes = sqlStmt.executeQuery(GetAllNotesQry);
            // Latest value will be at the top
            dbNotes.afterLast();

            while (dbNotes.previous()) {
                notes.add(dbNotes.getString(2));
            }
            
            sqlStmt.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(DbHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return notes;
    }

    @Override
    public boolean save(String note) {
        try {
            // Open connection
            Connection conn = DriverManager.getConnection(JDBC_URL, USER_NAME, USER_PASSWORD);

            String saveNoteQry = "INSERT INTO " + NOTES_TABLE + " (note) VALUES ('" + note + "')";
            Statement sqlStmt = conn.createStatement();

          sqlStmt.execute(saveNoteQry);

            sqlStmt.close();
            conn.close();

            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DbHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public boolean delete(int noteIndex) {
        try {
            // Open connection
            Connection conn = DriverManager.getConnection(JDBC_URL, USER_NAME, USER_PASSWORD);

            String saveNoteQry = "DELETE FROM " + NOTES_TABLE + " WHERE NoteId = " + noteIndex;
            Statement sqlStmt = conn.createStatement();

            ResultSet dbNotes = sqlStmt.executeQuery(saveNoteQry);

            sqlStmt.close();
            conn.close();

            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DbHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public boolean isEmpty() {
        return getNotes().isEmpty();
    }

}
