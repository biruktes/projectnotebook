package Models;

import java.util.ArrayList;

/**
 * The main application, includes all components and events
 *
 * @since 19/10/2017
 * @author Bereketab Gulai
 */
public interface DbHandlable {

    /**
     * @return returns the all notes
     */
    public ArrayList<String> getNotes();

    /**
     * Saves the notes to the db
     *
     * @return boolean is returns to indicate success/failure
     * @param note is added notes
     */
    public boolean save(String note);

    /**
     * Takes index/id and removes it from the db
     *
     * @return boolean is returns to indicate success/failure
     * @param noteIndex used to access the item to be removed
     */
    public boolean delete(int noteIndex);
}
